package dev.ecbn.tiketdamri.model

import com.google.gson.annotations.SerializedName

data  class TokenResponse(
	@field:SerializedName("token")
	var token: String? = null
)

data class OriginResponse(

	@field:SerializedName("response_code")
	val responseCode: String? = null,

	@field:SerializedName("data")
	val data: List<OriginData>,

	@field:SerializedName("status")
	val status: String? = null
)

data class OriginData(

	@field:SerializedName("asal")
	val asal: String? = null,

	@field:SerializedName("latloc")
	val latloc: String? = null,

	@field:SerializedName("nm_asal")
	val nmAsal: String? = null,

	@field:SerializedName("nm_kota")
	val nmKota: String? = null,

	@field:SerializedName("longloc")
	val longloc: String? = null,

	@field:SerializedName("alamat")
	val alamat: String? = null
)