package dev.ecbn.tiketdamri.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dev.ecbn.tiketdamri.data.NetworkState
import dev.ecbn.tiketdamri.data.Status
import dev.ecbn.tiketdamri.data.TiketRepository
import dev.ecbn.tiketdamri.model.OriginData
import dev.ecbn.tiketdamri.model.TokenResponse
import kotlinx.coroutines.launch

/**
 * Tiket Damri Created by ecbn on 05/08/20.
 */
class TiketViewModel(
    private val repository: TiketRepository
) : ViewModel() {
    private val _networkState = MutableLiveData<NetworkState>()
    private val _tokenState = MutableLiveData<TokenResponse>()
    private val _originState = MutableLiveData<List<OriginData>>()

    val network: LiveData<NetworkState>
        get() = _networkState
    val token: LiveData<TokenResponse>
        get() = _tokenState
    val origin: LiveData<List<OriginData>>
        get() = _originState

    fun getToken() {
        viewModelScope.launch {
            runCatching {
                _networkState.postValue(NetworkState.LOADING)
                repository.getToken()
            }.onSuccess {
                _networkState.postValue(NetworkState.LOADED)
                _tokenState.postValue(it)
            }.onFailure {
                _networkState.postValue(NetworkState(Status.FAILED, it.message.toString()))
            }
        }
    }

    fun getOrigin(token: String) {
        viewModelScope.launch {
            runCatching {
                _networkState.postValue(NetworkState.LOADING)
                repository.getOrigin(token)
            }.onSuccess {
                _networkState.postValue(NetworkState.LOADED)
                _originState.postValue(it.data)
            }.onFailure {
                _networkState.postValue(NetworkState(Status.FAILED, it.message.toString()))
            }
        }
    }
}