package dev.ecbn.tiketdamri.module

import dev.ecbn.tiketdamri.vm.TiketViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { TiketViewModel(get()) }
}