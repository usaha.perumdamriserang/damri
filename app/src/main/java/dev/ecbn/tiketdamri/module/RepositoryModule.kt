package dev.ecbn.tiketdamri.module

import dev.ecbn.tiketdamri.data.TiketRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { TiketRepository(get()) }
}