package dev.ecbn.tiketdamri.data

import android.util.Log
import dev.ecbn.tiketdamri.model.OriginResponse
import dev.ecbn.tiketdamri.model.TokenResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class TiketRepository(
    private val serviceInterface: ServiceInterface
) : CoroutineScope {

    private val TAG = TiketRepository::class.java.simpleName
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    suspend fun getToken(): TokenResponse {
        Log.d("TAG", "getTOken")
        return withContext(Dispatchers.IO) {
            return@withContext serviceInterface.getToken()
        }
    }

    suspend fun getOrigin(token: String): OriginResponse {
        Log.d("TAG", "getOrigin")
        return  withContext(Dispatchers.IO){
            return@withContext serviceInterface.getOrigin(token)
        }
    }
}