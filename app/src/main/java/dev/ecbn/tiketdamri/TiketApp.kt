package dev.ecbn.tiketdamri

import android.app.Application
import dev.ecbn.tiketdamri.module.repositoryModule
import dev.ecbn.tiketdamri.module.retrofitModule
import dev.ecbn.tiketdamri.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Tiket Damri Created by ecbn on 05/08/20.
 */
class TiketApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@TiketApp)
            modules(
                listOf(
                    retrofitModule,
                    viewModelModule,
                    repositoryModule
                )
            )
        }
    }
}