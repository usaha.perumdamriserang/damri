package dev.ecbn.tiketdamri.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import dev.ecbn.tiketdamri.R
import dev.ecbn.tiketdamri.data.Status
import dev.ecbn.tiketdamri.model.OriginData
import dev.ecbn.tiketdamri.util.hide
import dev.ecbn.tiketdamri.util.show
import dev.ecbn.tiketdamri.util.showError
import dev.ecbn.tiketdamri.vm.TiketViewModel
import kotlinx.android.synthetic.main.activity_main.*
import net.idik.lib.slimadapter.SlimAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mTiketViewModel: TiketViewModel by viewModel()

    private var mOriginAdapter = SlimAdapter.create()
        .register<OriginData>(R.layout.item_origin) { data, injector ->
            injector.text(R.id.tvKota, data.nmKota)
            injector.text(R.id.tvAsal, data.nmAsal)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        observeNetwork()
        observeToken()
        observeOrigin()

        mTiketViewModel.getToken()
    }

    private fun observeOrigin() {
        mTiketViewModel.origin.observe(this, Observer {
            mOriginAdapter.updateData(it)
        })
    }

    private fun observeToken() {
        mTiketViewModel.token.observe(this, Observer {
            if (it.token != null){
                mTiketViewModel.getOrigin(it.token.toString())
            }
        })
    }

    private fun observeNetwork() {
        mTiketViewModel.network.observe(this, Observer { t ->
            srlOrigin.isRefreshing = t?.status == Status.RUNNING
            when (t?.status) {
                Status.NO_RESULT -> {
                    this.showError(t.msg)
                    tvErrorMessage.show()
                    tvErrorMessage.text = t.msg
                }
                Status.RUNNING -> {
                    tvErrorMessage.hide()
                }
                Status.FAILED -> {
                    this.showError(t.msg)
                    tvErrorMessage.show()
                    tvErrorMessage.text = t.msg
                }
                Status.SUCCESS -> {
                    tvErrorMessage.hide()
                }
            }
        })
    }

    private fun initView() {
        rvOrigin.layoutManager = LinearLayoutManager(this)
        rvOrigin.adapter = mOriginAdapter
    }
}