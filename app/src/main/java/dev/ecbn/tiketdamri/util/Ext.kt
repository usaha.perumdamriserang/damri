package dev.ecbn.tiketdamri.util

import android.content.Context
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast

/**
 * Tiket Damri Created by ecbn on 05/08/20.
 */

fun Context.showError(msg: String){
    Toast.makeText(applicationContext, msg, Toast.LENGTH_SHORT).show()
}

fun View.hide(){
    this.visibility =  GONE
}
fun View.show(){
    this.visibility =  VISIBLE
}